console.log("Hello World!");

const proxy = "https://cors-anywhere.herokuapp.com/"
const url = "https://quotes.informatika.app/"

function logout() {
    localStorage.clear()
    alert("Success Logout")
}

function updateQuoteModal(eventObject, formId) {
    $(formId+" input[name=token]").val(localStorage.getItem('token'))
}

function postRegister(formId) {
    var data = $(formId).serializeArray()

    $.ajax({
        url: url+"/auth/registration",
        method: "POST",
        data: {
            class_id: data[0].value,
            registration_number: data[1].value,
            name: data[2].value,
            email: data[3].value,
            password: data[4].value
        }
    })
    .done((data) => {
        alert(data)
    })
    .fail((xhr) => {
        alert(xhr.responseText)
    })
}

function postLogin(formId) {
    var data = $(formId).serializeArray()

    $.ajax({
        url: url+"/auth/login",
        method: "POST",
        data: {
            registration_number: data[0].value,
            password: data[1].value
        }
    })
    .done((data) => {
        localStorage.setItem('token', data['token'])
        alert(JSON.stringify(data))
    })
    .fail((xhr) => {
        alert(xhr.responseText)
    })
}

function postCreateQuote(formId) {
    var data = $(formId).serializeArray()

    $.ajax({
        url: proxy+url+"api/v1/quotes",
        method: "POST",
        headers: {
            "Authorization": "Bearer "+localStorage.getItem('token')
        },
        data: {
            name: data[2].value,
            description: data[3].value,
            // image: data[4].value,
        }
    })
    .done((data) => {
        console.log(data)
        alert(JSON.stringify(data))
    })
    .fail((xhr) => {
        alert(xhr.responseText)
    })
}

function getGlobalQuotes() {
    $("#quotes-cards").empty()

    $.ajax({
        url: proxy+url+"api/v1/quotes",
        method: "GET",
        headers: {
            "Authorization": "Bearer "+localStorage.getItem('token')
        }
    })
    .done((data) => {
        console.log(data)
        data.quotes.forEach((v) => {
            $("#quotes-cards").append(createQuoteCard(v))
        })
    })
    .fail((xhr) => {
        console.log(xhr)
        alert(xhr.responseText)
    })
}

function createQuoteCard(data) {
    return `
    <div class="col">
        <div class="card shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">
                <title>Placeholder</title>
                <rect width="100%" height="100%" fill="#55595c"/>
                <text x="50%" y="50%" fill="#eceeef" dy=".3em">${data.class_academic_year}</text>
            </svg>

            <div class="card-body">
            <figure class="text-center">
                <blockquote class="blockquote">
                    <p>${data.quote_description}</p>
                </blockquote>
                <figcaption class="blockquote-footer">
                    ${data.user_name} in <cite title="${data.quote_name}">${data.quote_name}</cite>
                </figcaption>
            </figure>
                <h5 class="card-title"></h5>
                <p class="card-text">
                    <span class="badge rounded-pill bg-primary">${data.class_name}</span>
                    <span class="badge rounded-pill bg-success">${data.quote_id}</span>
                    <span class="badge rounded-pill bg-danger">${data.user_email}</span>
                    <span class="badge rounded-pill bg-warning">${data.user_registration_number}</span>
                    <span class="badge rounded-pill bg-dark">${data.class_academic_year}</span>
                    <span class="badge rounded-pill bg-light text-dark">Light</span>
                </p>
            </div>

            <div class="card-footer">
                <div class="d-flex justify-content-between align-items-center">
                    <small class="text-muted">${data.created_at}</small>
                    
                    <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                    <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
}