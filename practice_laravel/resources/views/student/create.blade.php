@extends('layout')
    
@section('title', 'Pendaftaran Mahasiswa')

@section('content')
    <div class="container pt-4 bg-white">
        <div class="row">
            <div class="col-md-8 col-xl-6">
                <h1>Pendaftaran Mahasiswa</h1>
                <hr>
                <form action="{{ route('student.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf 
                    <div class="form-group">
                        <label for="nim">NIM </label>
                        <input type="text"class="form-control @error('nim') is-invalid @enderror"id="nim"name="nim"value="{{old('nim')}}">
                        @error('nim') 
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nama">NamaLengkap </label>
                        <input type="text"class="form-control @error('nama') is-invalid @enderror"id="nama"name="nama"value="{{old('nama')}}">
                        @error('nama') 
                            <div class="text-danger">{{$message}}</div>
                        @enderror 
                    </div>

                    <div class="form-group">
                        <label>JenisKelamin</label>
                        <div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jenis_kelamin" id="laki_laki" value="L" {{old('jenis_kelamin')=='L'?'checked':''}}>
                                <label class="form-check-label" for="laki_laki">Laki-laki </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jenis_kelamin" id="perempuan" value="P" {{old('jenis_kelamin')=='P'?'checked':''}}>
                                <label class="form-check-label" for="perempuan">Perempuan </label>
                            </div>
                            @error('jenis_kelamin') 
                                <div class="text-danger">{{$message}}</div>
                            @enderror 
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="jurusan">Jurusan </label>
                        <select class="form-control"name="jurusan"id="jurusan">
                            <option value="TeknikInformatika"{{old('jurusan')=='TeknikInformatika'?'selected':''}}>TeknikInformatika </option>
                            <option value="SistemInformasi"{{old('jurusan')=='SistemInformasi'?'selected':''}}>SistemInformasi </option>
                            <option value="IlmuKomputer"{{old('jurusan')=='IlmuKomputer'?'selected':''}}>IlmuKomputer </option>
                            <option value="TeknikKomputer"{{old('jurusan')=='TeknikKomputer'?'selected':''}}>TeknikKomputer </option>
                            <option value="TeknikTelekomunikasi"{{old('jurusan')=='TeknikTelekomunikasi'?'selected':''}}>TeknikTelekomunikasi </option>
                        </select>
                        @error('jurusan') 
                            <div class="text-danger">{{$message}}</div>
                        @enderror 
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat </label>
                        <textarea class="form-control"id="alamat"rows="3"name="alamat">{{old('alamat')}}
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label for="image">GambarProfile </label>
                        <input type="file"class="form-control-file"id="image"name="image">
                        @error('image') 
                            <div class="text-danger">{{$message}}</div>
                        @enderror 
                    </div>

                    <button type="submit" class="btn btn-primary mb-2">Daftar </button>
                </form>
            </div>
        </div>
    </div>
@endsection
