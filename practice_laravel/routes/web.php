<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [UserController::class, 'index'])->name('login.index');
Route::get('/logout', [UserController::class, 'logout'])->name('login.logout');
Route::post('/login', [UserController::class, 'login'])->name('login.process');

Route::get('/student', [StudentController::class, 'index'])->name('student.index')->middleware('login_auth');
Route::get('/student/create', [StudentController::class, 'create'])->name('student.create')->middleware('login_auth');
Route::post('/student/store', [StudentController::class, 'store'])->name('student.store')->middleware('login_auth');
Route::get('student/{student}', [StudentController::class, 'show'])->name('student.show')->middleware('login_auth');
Route::get('student/{student}/edit', [StudentController::class, 'edit'])->name('student.edit')->middleware('login_auth');
Route::patch('student/{student}', [StudentController::class, 'update'])->name('student.update')->middleware('login_auth');
Route::delete('student/{student}', [StudentController::class, 'destroy'])->name('student.destroy')->middleware('login_auth');