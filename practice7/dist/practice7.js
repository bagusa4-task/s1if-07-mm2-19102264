console.log("Hello World")

// Tools
class Tools {
    // string to binary
    static utf8_to_b64( str ) {
        return window.btoa(unescape(encodeURIComponent( str )));
    }
    
    // binary to string
    static b64_to_utf8( str ) {
        return decodeURIComponent(escape(window.atob( str )));
    }

    // encode data object to a string
    static encode( data ) {
        return this.utf8_to_b64(JSON.stringify( data ))
    }

    // decode string to an object
    static decode( str ) {
        return JSON.parse(this.b64_to_utf8( str ))
    }

    // current data with localStorage
    static current(key = "selected", value = null) {
        if(value != null) {
            localStorage.setItem(key, value)
        } else {
            return localStorage.getItem(key)
        }

        return this.current(key)
    }
}


// Common Part
const url = "https://quotesbeta.informatika.app/"
const proxy = "https://cors-anywhere.herokuapp.com/"

function getLoginData(event) {
    event.preventDefault()
    var nim = event.target.elements.nim.value

    $.ajax({
        type: "GET",
        url: proxy+url+"index.php/student/q/nim/"+nim,
    })
    .fail(xhr => {
        alert(xhr.responseText)
    })
    .done(data => {
        var student = JSON.stringify(data.student)

        if(data !== "" && data.student.length !== 0) {
            localStorage.setItem('student', student)
            alert(localStorage.getItem('student'))
        } else {
            alert("Re-Login")
        }

        getMyQuotes()
    })
}
function isLogin() {
    var student = localStorage.getItem('student')
    if(student == undefined || student.length == 0 || student == null) {
        $("#loginModal").modal('show')
        return false
    } else {
        $("#loginModal").modal('hide')
        return true
    }
}
function getStudent() {
    return isLogin() ? JSON.parse(localStorage.getItem('student'))[0] : false
}
function logout() {
    if(isLogin()) {
        localStorage.clear()
        alert("Success")
    } else {
        isLogin()
    }
}


// AJAX/Data Transaction
function getGlobalQuotes() {
    $.ajax({
        type: "GET",
        url: url+"index.php/quotes",
    })
    .fail(xhr => {
        alert(xhr.responseText)
    })
    .done(data => {
        // data = JSON.parse(data)
        generateQuotes(data)
    })
}
function getClassQuotes() {
    if(isLogin()) {
        $.ajax({
            type: "GET",
            url: url+"index.php/quotes/q/class_id/"+getStudent().class_id,
        })
        .fail(xhr => {
            alert(xhr.responseText)
        })
        .done(data => {
            // data = JSON.parse(data)
            generateQuotes(data)
        })
    }
}
function getMyQuotes() {
    if(isLogin()) {
        $.ajax({
            type: "GET",
            url: url+"index.php/quotes/q/my_quote/"+getStudent().nim,
        })
        .fail(xhr => {
            alert(xhr.responseText)
        })
        .done(data => {
            // data = JSON.parse(data)
            generateQuotes(data)
        })
    }
}
function postCreateQuote(data) {
    if(isLogin()) {
        return $.ajax({
            type: "POST",
            url: url+"index.php/quotes/",
            data: data,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: (xhr) => {
                xhr.overrideMimeType("text/plain; charset=x-user-defined")
            }
        })
    }
}
function postUpdateQuote(data, quoteId) {
    if(isLogin()) {
        return $.ajax({
            type: "POST",
            url: url+"index.php/quotes/q/edit/"+quoteId,
            data: data,
            dataType: "JSON",
            processData: false,
            contentType: false,
            beforeSend: (xhr) => {
                xhr.overrideMimeType("text/plain; charset=x-user-defined")
            }
        })
    }
}
function deleteQuote(data) {
    if(isLogin()) {
        return $.ajax({
            type: "DELETE",
            url: url+"index.php/quotes/q/quote_id/"+data.quote_id,
            beforeSend: (xhr) => {
                xhr.overrideMimeType("text/plain; charset=x-user-defined")
            }
        })
    }
}


// UI UX
function showAboutModal() {
    var modal = $("#generalViewModal")
    var body = modal.find("div.modal-body")

    var quote = `
    <figure class="text-center">
        <blockquote class="blockquote">
            <p>Since There is no password for login, You Can Use only NIM to do CRUD</p>
            <p>Do with your own responsibility</p>
        </blockquote>
        <figcaption class="blockquote-footer">
            19102264 in <cite title="Quote">Quote</cite>
        </figcaption>
    </figure>
    `
    body.html(quote)

    modal.modal('show')
}
function whoAmIModal() {
    var modal = $("#whoAmIModal")
    if(!isLogin()) {
        modal.modal('hide')
    } else {
        var student = getStudent()
        var form = $("#whoAmIModal form")
        form.find("input[name='nim']").val(student.nim)
        form.find("input[name='name']").val(student.name)
        form.find("input[name='gender']").val(student.gender)
        form.find("input[name='studentId']").val(student.student_id)
        form.find("input[name='classId']").val(student.class_id)
        form.find("input[name='className']").val(student.class_name)
        modal.modal('show')
    }
}
function previewImage(file, previewId) {
    if(file) {
        var reader = new FileReader()
        var view = $(previewId)
        reader.onloadstart = (e) => {
            view.removeAttr('hidden')
            view.show()
            view.find('img').prop('hidden', true)
            view.append("<span>Loading ....</span>")
        }
        reader.onload = (e) => {
            view.find("span").remove()
            view.find('img').attr("src", e.target.result)
            view.find('img').removeAttr('hidden')
        }
        reader.readAsDataURL(file)
    }
}
function createQuoteModal() {
    var student = getStudent()
    var modal = $("#quoteModal")
    var form = modal.find("form#quoteForm")
    var body = modal.find("div.modal-body")

    if(!isLogin()) {
        modal.modal('hide')
    } else {
        modal.modal('show')
    }

    modal.find("h5#quoteModalLabel").text("Create Quote")
    body.find("input[name='imagename']").on('change', (e) => {
        previewImage(e.target.files[0], "#preview")
    })
    body.find("input[name='name']").val(student.name)
    body.find("input[name='nim']").val(student.nim)
    body.find("input[name='student_id']").val(student.student_id)
    body.find("input[name='class']").val(student.class_name)
    form.on('submit', (e) => {
        e.preventDefault()
        form.find("#action").prop('disabled', true)

        var data = new FormData(e.target)
        postCreateQuote(data)
        .fail(xhr => {
            console.log(xhr)
            alert(xhr.responseText)
        })
        .done(data => {
            console.log(data)
            getMyQuotes()
            modal.modal('hide')
            form.find("#action").prop('disabled', false)
        })

        e.stopImmediatePropagation(); // to prevent more than once submission
        return false; 
    })
}
function editQuoteModal(that) {
    var data = Tools.decode($(that).closest(".card")[0].dataset.card)    
    var modal = $("#quoteModal")
    var form = modal.find("form#quoteForm")
    var body = modal.find("div.modal-body")

    if(!isLogin()) {
        modal.modal('hide')
    } else {
        modal.modal('show')
    }

    modal.find("h5#quoteModalLabel").text("Edit Quote")
    body.find("input[name='quote_id']").val(data.quote_id)
    body.find("input[name='title']").val(data.title)
    body.find("textarea[name='description']").val(data.description)

    var preview = body.find("div#preview")
    preview.removeAttr("hidden")
    preview.find("img").attr("src", data.images)
    preview.removeAttr("hidden")
    body.find("input[name='imagename']").on('change', (e) => {
        previewImage(e.target.files[0], "#preview")
    })

    body.find("input[name='name']").val(data.name)
    body.find("input[name='nim']").val(data.nim)
    body.find("input[name='student_id']").val(data.student_id)
    body.find("input[name='class']").val(data.class)
    body.find("input[name='created']").val(data.created)
    body.find("input[name='updated']").val(data.updated)
    form.on('submit', (e) => {
        e.preventDefault()
        form.find("#action").prop('disabled', true)

        var data = new FormData(e.target)
        postUpdateQuote(data, data.quote_id)
        .fail(xhr => {
            console.log(xhr)
            alert(xhr.responseText)
        })
        .done(data => {
            console.log(data)
            getMyQuotes()
            modal.modal('hide')
            form.find("#action").prop('disabled', false)
        })

        e.stopImmediatePropagation(); // to prevent more than once submission
        return false; 
    })
}
function deleteQuoteModal(that) {
    var data = Tools.decode($(that).closest(".card")[0].dataset.card)
    var modal = $("#generalViewModal")
    var body = modal.find("div.modal-body")
    
    body.text(`"Are You Sure Delete |${data.quote_id} - ${data.title} by ${data.name}| ?"`)

    var btnAction = modal.find("button#action")
    btnAction.text("Delete !")
    btnAction.addClass("btn-danger")
    btnAction.removeClass("btn-primary d-none")
    btnAction.removeAttr("hidden")

    btnAction.on("click", (e) => {
        deleteQuote(data)
        .fail(xhr => {
            console.log(xhr)
            alert(xhr.responseText)
        })
        .done(data => {
            console.log(data)
            getMyQuotes()
            modal.modal('hide')
        })
    })

    modal.on("hide.bs.modal", (e) => {
        btnAction.prop("hidden", true)
    })

    modal.modal("show")
}
function quoteViewModal(that) {
    var data = Tools.decode($(that).closest(".card")[0].dataset.card)
    var modal = $("#generalViewModal")
    var body = modal.find("div.modal-body")
    body.empty()
    body.html(createQuoteCard(data, false))
    modal.modal("show")
}


function generateQuotes(data) {
    console.log(data)
    var quotes = $("#card-quotes")
    quotes.empty()
    data.quotes.forEach(v => {
        quotes.append(createQuoteCard(v))
    })
}
function replaceWithThumbnail(that) {
    var thumbnail = `
    <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">
        <title>Placeholder</title>
        <rect width="100%" height="100%" fill="#55595c"/>
        <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
    </svg>
    `

    $(that).replaceWith(thumbnail)
}
function createQuoteCard(data, card = true) {    
    var image = `
        <img class="card-img-top" width="100%" height="255" role="img" 
        onerror="replaceWithThumbnail(this)"
        src="${data.images}"></img>
    `

    var badge = `
        <span class="badge bg-secondary">${data.quote_id}</span>
        <span class="badge bg-primary">${data.class}</span>
        <span class="badge bg-success">${data.nim}</span>
        <span class="badge bg-danger">${data.student_id}</span>
        <span class="badge bg-warning text-dark">${data.updated}</span>
        <span class="badge bg-dark">${data.created}</span>
    `

    var menu = `
        <div class="btn-group btn-group-sm">
            <button type="button" 
                class="btn btn-sm btn-outline-primary"
                onclick="quoteViewModal(this)">View</button>
            ${ data.student_id === getStudent().student_id ?
            `<button type="button" 
                class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-split" 
                data-bs-toggle="dropdown" aria-expanded="false">
                <span class="visually-hidden">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu dropdown-menu-dark">
                <li><a class="dropdown-item" href="#" onclick="editQuoteModal(this)">Edit</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#" onclick="deleteQuoteModal(this)">Delete</a></li>
            </ul>` : "" }
        </div>
    `

    return `
    <div class="col">
        <div class="card shadow-sm" data-card="${Tools.encode(data)}">
            ${ image }
            <div class="card-body">
                <!-- <h5 class="card-title"></h5> -->
                <figure class="card-text text-center">
                    <blockquote class="blockquote">
                        <p>${data.description}</p>
                    </blockquote>
                    <figcaption class="blockquote-footer">
                        ${data.name} in <cite title="${data.title}">${data.title}</cite>
                    </figcaption>
                </figure>
                <!-- <p class="card-text"></p> -->
            </div>
            <div class="card-footer text-muted">
                <div class="${ card? 'd-flex' : '' } justify-content-between align-items-center">
                    ${ card? menu : "" }
                    ${ !card? badge : `<small class='text-muted'>${ data.updated }</small>` }
                </div>
            </div>
        </div>
    </div>
    `
}