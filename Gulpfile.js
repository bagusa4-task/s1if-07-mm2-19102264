const {src, dest, series} = require('gulp')

function copy(cb) {
    src([
        "./node_modules/bootstrap/dist/css/bootstrap.min.css",
        "./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js",
        "./node_modules/jquery/dist/jquery.min.js",
    ]).pipe(dest("./practice6/dist/"))

    src([
        "./node_modules/bootstrap/dist/css/bootstrap.min.css",
        "./node_modules/bootstrap/dist/js/bootstrap.bundle.min.js",
        "./node_modules/jquery/dist/jquery.min.js",
        "./node_modules/@fortawesome/fontawesome-free/css/all.min.css",
        "./node_modules/@fortawesome/fontawesome-free/js/all.min.js",
        "./node_modules/masonry-layout/dist/masonry.pkgd.min.js",
    ]).pipe(dest("./practice7/dist/"))

    cb()
}

function defaultTask(cb) {
    cb()
}

exports.default = series([
    defaultTask,
    copy
])