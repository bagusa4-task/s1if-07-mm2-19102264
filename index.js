console.log("Hello World!");

function getPractices(tableId) {
    var table = $(tableId).find("tbody")
    var practices = [
        "practice1",
        "practice2",
        "practice3",
        "practice4",
        "practice5",
        "practice6",
        "practice7",
        "practice_laravel/public"
    ]

    for(var i=0; i<practices.length; i++) {
        table.append(`
            <tr>
                <td>${i+1}</td>
                <td>
                    <a href="${window.location.href}/${practices[i]}">${practices[i]}</a>
                </td>
            </tr>
        `)
    }
}